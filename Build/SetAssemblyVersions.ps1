param(
        [parameter(mandatory=$true)][string]$VersionFile,
        [parameter(mandatory=$true)][int]$MajorVersion,
        [parameter(mandatory=$true)][int]$MinorVersion,
        [parameter(mandatory=$true)][int]$Patch,
        [parameter(mandatory=$true)][int]$Revision,
        [parameter(mandatory=$false)][string]$SemanticVersionSuffix
)

# NOTE: expects the following in the AssemblyInfo.cs:
# [assembly: AssemblyVersion("0.0.0.0")]
# [assembly: AssemblyInformationalVersion("development")]

#validate source parameters
if(($MajorVersion, $MinorVersion, $Patch, $Revision | ? { $_ -lt 0 -or $_ -gt 65535 }).length -gt 0) {
    throw "Invalid version."
}
if(($Revision -eq 0 -and $SemanticVersionSuffix.length -ne 0) -or ($Revision -ne 0 -and $SemanticVersionSuffix.length -eq 0)) {
    throw "Semantic and dotted version mismatch."
}
if(-not (test-path $VersionFile)) {
    throw "Unable to find version file."
}
 
$dottedVersion = "$MajorVersion.$MinorVersion.$Patch.$Revision"
$semanticVersion = "$MajorVersion.$MinorVersion.$Patch"
if($SemanticVersionSuffix.length  -ne 0) {
    $semanticVersion += $SemanticVersionSuffix
}

Write-Host "Setting dotted version to $dottedVersion and semantic version to '$semanticVersion'."
 
(type $versionFile) -replace '0.0.0.0',$dottedVersion -replace 'development',$semanticVersion | Out-File -Encoding UTF8 $versionFile