﻿$scriptFolder = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

$ErrorActionPreference = 'Stop'

pushd (join-path -resolve $scriptFolder ..\Source)

#Build
msbuild LedApi.sln /p:Configuration=Release /m
if(!$?) {
    throw "Build failed."
}

#Run tests
mstest /testcontainer:LedApiTests\bin\Release\LedApiTests.dll
if(!$?) {
    throw "Tests failed."
}

#build NuGet package
& ".nuget\NuGet.exe" pack LedApi\LedApi.csproj -Prop Configuration=Release -OutputDirectory LedApi\bin\Release
if(!$?) {
    throw "Failed to build NuGet package."
}

popd