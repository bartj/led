﻿$scriptFolder = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

$ErrorActionPreference = 'Stop'

pushd (join-path -resolve $scriptFolder ..\Source)

#Read latest version tag
$tag = git describe --match v* --tags
$parts = ($tag).split('-') 
if($parts[0] -notmatch '^v(\d+)\.(\d+)\.(\d+)$') {
    throw "Invalid tag format. Expected MAJOR.MINOR.PATCH format."
}
$major = [int]$Matches[1]
$minor = [int]$Matches[2]
$patch = [int]$Matches[3]
$isReleaseVersion = $parts.length -eq 1
if($isReleaseVersion) {
    $revision = 0
    $semanticVersionSuffix = ''
} else {
    $patch++
    $revision = $parts[1]
    $semanticVersionSuffix = '-build-' + $parts[1]
}

#Set assembly versions
dir -r -i AssemblyInfo.cs | % {
    & "$scriptFolder\SetAssemblyVersions" $_ $major $minor $patch $revision $semanticVersionSuffix
}

#run the build
& (join-path -Resolve $scriptFolder Build.ps1)

#if this is a release version (i.e. currently tagged), publish to NuGet
if($isReleaseVersion) {
    echo "Publishing package to NuGet..."
    .nuget\nuget.exe push (dir LedApi\bin\Release\LedApi.*.nupkg).FullName
    if(!$?) {
        throw "Failed to push package to NuGet.org."
    }
}

popd