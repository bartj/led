﻿$buildFolder = join-path $env:temp Build\Led
$scriptFolder = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

$ErrorActionPreference = 'Stop'

pushd

#Get latest and ensure folder is clean
if(test-path $buildFolder) {
    cd $buildFolder
    git clean -d -x -f
    git reset --hard
    git pull
}
else {
    md $buildFolder
    cd $buildFolder
    git clone https://bartj@bitbucket.org/bartj/led.git .
}
cd Build

.\BuildAndPackage.ps1

popd