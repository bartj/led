A .NET API for the B1248 LED name badge.

Note that a compiled version of this code is available as a NuGet package:

https://www.nuget.org/packages/LedApi/
