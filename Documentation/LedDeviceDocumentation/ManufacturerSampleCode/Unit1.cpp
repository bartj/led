//---------------------------------------------------------------------------

#include <vcl.h>
#include <windows.h>
#include "pcomm.h"
#pragma hdrstop
//---------------------------------------------------------------------------
//   Important note about DLL memory management when your DLL uses the
//   static version of the RunTime Library:
//
//   If your DLL exports any functions that pass String objects (or structs/
//   classes containing nested Strings) as parameter or function results,
//   you will need to add the library MEMMGR.LIB to both the DLL project and
//   any other projects that use the DLL.  You will also need to use MEMMGR.LIB
//   if any other projects which use the DLL will be performing new or delete
//   operations on any non-TObject-derived classes which are exported from the
//   DLL. Adding MEMMGR.LIB to your project will change the DLL and its calling
//   EXE's to use the BORLNDMM.DLL as their memory manager.  In these cases,
//   the file BORLNDMM.DLL should be deployed along with your DLL.
//
//   To avoid using BORLNDMM.DLL, pass string information using "char *" or
//   ShortString parameters.
//
//   If your DLL uses the dynamic version of the RTL, you do not need to
//   explicitly add MEMMGR.LIB as this will be done implicitly for you
//---------------------------------------------------------------------------

#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
        return 1;
}
//---------------------------------------------------------------------------
extern "C" _declspec(dllexport) int _stdcall SelectRunFile(int ComPort,unsigned char MsgProgramByte)
{
  unsigned char Buff[10];
  Buff[0]=0x02;
  Buff[1]=0x33;
  Buff[2]=MsgProgramByte;
  if (sio_open(ComPort)<0)
    return 1;
  sio_ioctl(ComPort,B38400,P_NONE|BIT_8|STOP_1);
  sio_SetReadTimeouts(ComPort,10,10); //

  sio_write(ComPort,Buff,3);
  sio_close(ComPort);
  return 0;
}
//---------------------------------------------------------------------------
extern "C" _declspec(dllexport) int _stdcall SendData(int ComPort,int MsgId, unsigned char Speed, unsigned char Action,unsigned char * Buffer, int Length)
{
  unsigned char Buff[0x1000];
  unsigned char Head[4][4];
  unsigned char CheckSum[4];
  for (int i=0;i<0x1000;i++)
    Buff[i]=0x00;
  for (int i=0;i<4;i++)
  {
    Head[i][0]=0x02;
    Head[i][1]=0x31;//address 0
    Head[i][2]=0x06+MsgId;//command
    Head[i][3]=i*0x40;//address 1
    CheckSum[i]=Head[i][1]+Head[i][2]+Head[i][3];
  }
  Buff[0]=Speed;
  Buff[1]=0x31;
  Buff[2]=Action;
  Buff[3]=Length;

  for (int i=0;i<Length;i++)
    Buff[4+i]=Buffer[i];
  // check sum
  for (int i=0;i<64;i++)
  {
    CheckSum[0]+=Buff[0+i];
    CheckSum[1]+=Buff[64+i];
    CheckSum[2]+=Buff[128+i];
    CheckSum[3]+=Buff[192+i];
  }

 if (sio_open(ComPort)<0)
   return 1;
  sio_ioctl(ComPort,B38400,P_NONE|BIT_8|STOP_1);
  sio_write(ComPort,Head[0],4);
  sio_write(ComPort,Buff,64);
  sio_write(ComPort,CheckSum,1);
  Sleep(100);
  if (Length>50)
  {
    sio_write(ComPort,Head[1],4);
    sio_write(ComPort,Buff+64,64);
    sio_write(ComPort,CheckSum+1,1);
    Sleep(100);
  }
  if (Length>50+64)
  {
    sio_write(ComPort,Head[2],4);
    sio_write(ComPort,Buff+128,64);
    sio_write(ComPort,CheckSum+2,1);
    Sleep(100);
  }
  if (Length>50+128)
  {
    sio_write(ComPort,Head[3],4);
    sio_write(ComPort,Buff+192,64);
    sio_write(ComPort,CheckSum+3,1);
    Sleep(100);
  }
  sio_close(ComPort);
  return 0;
}

