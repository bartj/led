﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LedApi;

namespace LedConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var led = new B1248(new SerialCommunicationsPort(3));
            led.SetMessage("Test").Wait();
        }
    }
}
