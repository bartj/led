using System.Collections.Generic;
using System.Threading.Tasks;

namespace LedApi
{
    public interface ICommunicationsPort
    {
        Task Send(IEnumerable<byte> data);
    }
}