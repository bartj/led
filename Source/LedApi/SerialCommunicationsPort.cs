using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading.Tasks;

namespace LedApi
{
    public class SerialCommunicationsPort : ICommunicationsPort
    {
        readonly int _portNumber;

        public SerialCommunicationsPort(int portNumber)
        {
            _portNumber = portNumber;
        }

        public async Task Send(IEnumerable<byte> data)
        {
            await Task.Run(() =>
            {
                using (var port = new SerialPort("COM" + _portNumber))
                {
                    port.BaudRate = 38400;
                    port.Open();
                    byte[] buffer = data.ToArray();
                    port.Write(buffer, 0, buffer.Length);
                }
            });
        }
    }
}