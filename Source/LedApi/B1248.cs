﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LedApi
{
    /// <summary>
    /// Based on http://zunkworks.com/ProgrammableLEDNameBadges. See at bottom of file for the relevant content from this page.
    ///
    /// </summary>
    public class B1248
    {
        readonly ICommunicationsPort _port;

        public B1248(ICommunicationsPort port)
        {
            _port = port;
        }

        public async Task SetMessage(string text)
        {
            if(text.Length == 0)
            {
                Clear();
                return;
            }
                
            if (text.Length > 250)
                throw new ArgumentException("This device only supports messages up to 250 characters in length.");

            var data = new List<byte>();
            data.Add(0); //lead byte
            AddDataCommand(data, text);
            data.AddRange(new byte[] {2, 0x33, 1});
            await _port.Send(data.ToArray());
        }

        static void AddDataCommand(List<byte> data, string text)
        {
            var remainingBytes = new List<byte>();
            remainingBytes.AddRange(new byte[] { 0x35, 0x31, 0x42 });                           //speed setting, message number, style setting
            remainingBytes.Add(Convert.ToByte(text.Length));                                    //length of string
            remainingBytes.AddRange(Encoding.ASCII.GetBytes(text));                             //string bytes
            remainingBytes.AddRange(Enumerable.Repeat((byte)0, 256 - remainingBytes.Count));    //zero padding

            for (int i = 0; i != 4; ++i)
            {
                var thisChunk = remainingBytes.Take(64).ToArray();
                remainingBytes.RemoveRange(0, 64);

                data.AddRange(new byte[] {2, 0x31, 6, Convert.ToByte(0x40 * i)}); //command and address bytes
                data.AddRange(thisChunk);
                data.Add(Convert.ToByte(data.Skip(data.Count - 67).Take(67).Sum(v => (int) v) % 256));  //checksum
            }
        }

        public void Clear()
        {
            _port.Send(new byte[] {0, 2, 0x33, 0});
        }
    }
}

/*
Protocol Information:
 
Assuming 6 messages of 250chars and 2 images 12x1024pixels are sent:
 The software sends data in groups of 69 bytes except for the first and last groups. The first message consists of one byte can be thought of as an intialization byte, and is always 0x00. The last group consists of 3 bytes: 0x02, 0x33, 0x?????. The last byte seems to be a checksum since it does change depending on the length of data sent.-------
 
After the first byte (0x00) the software sends the
 Each group is divided into roughly 3 parts, Command/Address, Data and settings, and a checksum. The address/command part, always starts with 0x02 and is followed by 3 more bytes. The first of which is almost always 0x31, the only exception is the last data group sent. The last two bytes in this group appear to be an address, the first group is always 0x06 0x00 and each additional group increments by 0x40 or 64, one for each payload byte. 

After the command/address is sent, the data and settings part of the data group is sent, it always consists of 64 bytes. If a particular group doesn't have enough data to pack the entire 64byte block it is zero padded. If a particular group has more than 64 bytes it's wrapped into the next data group. If a message is set, 4 data groups are sent for each message. However, text message can only be 250 characters, the maximum number of data blocks is 4 or 256 bytes, the "extra" 6 bytes are used as follows. The first 4 bytes in the first data group of each message provide the Speed setting, Message number, Style setting and message length. The last two bytes of the last data group of each message are always 0x00 and 0x00, I'd guess the designer(s) padded out these last bytes to make the maximum message length a nice round number, 250. These 250 bytes store the message, using english character set, it's one byte for one character. The asian character sets require two bytes per character.
 
--------------------character set image---------------
 
Once the data and settings are sent, a check sum is sent. The checksum is easily calculated by adding up the last three bytes of the command/address data and the 64 byte data/settings part. The resulting sum is taken mod 256 to return an 1 byte number. It took me a while to figure out this check sum. Initially I thought it was a sum, however when I added up all of the bytes in a block I didn't get the correct number. Graphing the checksum values showed an obvious cyclic pattern, so this would probably throw out something complicated like a CRC, plus a CRC wouldn't really be nessary for a simple short serial link, also CRC would eat up too many resources to be of much benifit. Additional clues were if any byte in the message changed the check sum would change, if the address changed the checksum changed. At one point I thought it might be doing an XOR of all the bytes so I fiddled around with calculating XORs in excel but that didn't give the correct result either. I went back to looking at sums again since there was such an obvious pattern. When I summed the message bytes I noticed that the sum (0x89_would be close to the check sum (0x87) then it hit me, exclude the first byte 0x02 from the checksum, and voila it worked for every data group I had captured!
 
-------picture of cyclic checksum value graph-------
 
The data format for images was also an interesting challenge. It was fairly obvious which data groups were being used for images by sending known images, ie all black, or only first and last pixel black, etc... However the device was sending more data than was needed. For a 12x1024 pixel or 12,288 bit image, it was sending 31 groups of 64 bytes and one group of 56 bytes or 2040 bytes 16,320 bits of image data. By sending a couple specially crafted images I was able to figure out how it was sending the data.
 
---------example bmp's-------- and descriptions
 USB Pinouts 

USB Pinouts 


Image message settings and their address blocks, are they addresses?
 image checksums or the following 24 bytes, what are they?
 last byte of all data sent, what is it, why does it change?
 behavior for sending less than 6 messages and 2 images

 */